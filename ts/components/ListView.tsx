import React, { ReactElement } from 'react'
import { Block, Col, Row } from 'jsxstyle'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow'
import DragIndicatorIcon from '@material-ui/icons/DragIndicator'
import IconButton from '@material-ui/core/IconButton'
import RestoreIcon from '@material-ui/icons/Restore'
import { useList, useSelected } from '../utils/hooks'
import { Input } from './Input'
import { useDispatch } from 'react-redux'
import { useDrag, useDrop } from 'react-dnd'
import _ from 'lodash'
import {
    addNewEntry,
    addNewHeader,
    deleteEntry,
    deleteList,
    duplicateList,
    resetEntries,
    reorderEntries,
    setListTitle,
    setEntryDone,
    setEntryText,
} from '../utils/actions'

export const Entry = ({ id }: { id: string }): ReactElement | null => {
    const selected = useSelected()
    const list = useList(selected)

    const entry = list?.entries[id]

    const dispatch = useDispatch()
    const setDone = React.useCallback(
        (evt) => setEntryDone(dispatch, list!.id, id, evt.target.checked),
        [dispatch, list]
    )

    const setText = React.useCallback(
        (evt) => setEntryText(dispatch, list!.id, id, evt.target.value),
        [dispatch, list]
    )

    const deleteSelf = React.useCallback(() => {
        deleteEntry(dispatch, list!.id, id)
    }, [dispatch, list])

    const [{ isDragging }, drag, preview] = useDrag({
        item: { type: 'EntryItem', id },
        isDragging: (mon) => id === mon.getItem().id,
        collect: (mon) => ({
            isDragging: !!mon.isDragging(),
        }),
    })

    const [, drop] = useDrop({
        accept: 'EntryItem',
        hover: (item, mon) =>
            reorderEntries(dispatch, selected!, mon.getItem().id, id),
        collect: (monitor) => ({
            isOver: !!monitor.isOver(),
        }),
    })

    if (!selected || !list || !entry) {
        console.error('List or entry is invalid!')
        console.error(`list id: ${selected}, entry id: ${id}`)
        console.error('list:', list)
        console.error('entry:', entry)
        return null
    }

    return (
        <Row
            key={`entry${entry.id}`}
            props={{ ref: _.flow(preview, drop) }}
            visibility={isDragging ? 'hidden' : 'visible'}
            alignItems="center"
            margin={entry.header ? '0.3rem 0rem -0.5rem 0rem' : undefined}
            width="100%"
        >
            <Block props={{ ref: drag }}>
                <DragIndicatorIcon
                    fontSize="small"
                    style={{ cursor: 'move', color: '#aaa', display: 'block' }}
                />
            </Block>
            {entry.header ? (
                <Input
                    onChange={setText}
                    fontWeight="bold"
                    fontSize="x-large"
                    value={entry.text}
                    onDelete={deleteSelf}
                />
            ) : (
                <>
                    <Checkbox
                        checked={entry.done || false}
                        onChange={setDone}
                    />
                    <Input
                        onChange={setText}
                        onDelete={deleteSelf}
                        value={entry.text}
                        line
                    />
                </>
            )}
        </Row>
    )
}

export const ListView = (): ReactElement => {
    const selected = useSelected()
    const list = useList(selected)

    const dispatch = useDispatch()
    const setTitle = React.useCallback(
        (evt) => setListTitle(dispatch, list!.id, evt.target.value),
        [dispatch, selected]
    )

    const addEntry = React.useCallback(() => addNewEntry(dispatch, list!.id), [
        dispatch,
        selected,
    ])

    const addHeader = React.useCallback(
        () => addNewHeader(dispatch, list!.id),
        [dispatch, selected]
    )

    const undoAllEntries = React.useCallback(
        () => resetEntries(dispatch, list!.id),
        [dispatch, selected]
    )

    const deleteSelf = React.useCallback(() => deleteList(dispatch, list!.id), [
        dispatch,
        selected,
    ])

    const cloneList = React.useCallback(
        () => duplicateList(dispatch, list!.id),
        [dispatch, selected]
    )

    if (!list) return <div>no list</div>

    return (
        <Col padding="1rem">
            <Row borderBottom="1px solid #ccc" alignItems="center">
                <Input fontSize="2rem" onChange={setTitle} value={list.title} />
                <IconButton onClick={cloneList}>
                    <DoubleArrowIcon titleAccess="Clone" />
                </IconButton>
                <IconButton onClick={undoAllEntries}>
                    <RestoreIcon titleAccess="Uncomplete" />
                </IconButton>
                <IconButton onClick={deleteSelf}>
                    <DeleteForeverIcon titleAccess="Delete" />
                </IconButton>
            </Row>
            <Col overflowY="auto" paddingBottom="0.5rem">
                {list.order.map((id) => (
                    <Entry key={id} id={id} />
                ))}
            </Col>
            <Row marginTop="0.5rem">
                <Button variant="contained" onClick={addEntry}>
                    Add Entry
                </Button>
                <Block width="1rem" />
                <Button variant="contained" onClick={addHeader}>
                    Add Header
                </Button>
            </Row>
        </Col>
    )
}
