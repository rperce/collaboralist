import React, { ReactElement } from 'react'
import { Block } from 'jsxstyle'
import MaterialInput from '@material-ui/core/Input'
import IconButton from '@material-ui/core/IconButton'
import InputAdornment from '@material-ui/core/InputAdornment'
import CloseIcon from '@material-ui/icons/Close'

interface Props {
    onChange: (evt: React.ChangeEvent<HTMLInputElement>) => void
    onDelete?: () => void
    value: string
    line?: boolean
}

const preventDefault = (evt: React.MouseEvent): void => evt.preventDefault()
const DeleteEntry = ({ onDelete }: { onDelete: () => void }): ReactElement => (
    <IconButton onClick={onDelete} onMouseDown={preventDefault}>
        <CloseIcon fontSize="small" />
    </IconButton>
)
export const Input = ({
    onChange,
    onDelete,
    value,
    line,
    ...style
}: Props & React.CSSProperties): ReactElement => {
    const [focused, setFocused] = React.useState(false)
    const onFocus = () => setFocused(true)
    const onBlur = () => setFocused(false)
    return (
        <Block margin="0.3em" width="100%">
            <MaterialInput
                {...{ onChange, value, style, onFocus, onBlur }}
                fullWidth
                disableUnderline={!line}
                endAdornment={
                    onDelete && focused ? (
                        <InputAdornment position="end">
                            <DeleteEntry {...{ onDelete }} />
                        </InputAdornment>
                    ) : undefined
                }
            />
        </Block>
    )
}
