import React, { ReactElement } from 'react'
import Link from 'next/link'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import IconButton from '@material-ui/core/IconButton'
import Divider from '@material-ui/core/Divider'
import { Block, Col } from 'jsxstyle'
import { ListId } from '../utils/store'
import { useLists, useSelected, useOrder } from '../utils/hooks'
import { useDispatch } from 'react-redux'
import { useDrag, useDrop } from 'react-dnd'
import { addNewList, reorderLists } from '../utils/actions'
import _ from 'lodash'

interface ListItemProps {
    id: ListId
    title?: string
    selected: boolean
}

const ListItem = (props: ListItemProps): ReactElement => {
    const { id, selected, title } = props

    const [{ isDragging }, drag] = useDrag({
        item: { type: 'ListItem', id },
        isDragging: (mon) => id === mon.getItem().id,
        collect: (mon) => ({
            isDragging: !!mon.isDragging(),
        }),
    })

    const dispatch = useDispatch()
    const [, drop] = useDrop({
        accept: 'ListItem',
        hover: (item, mon) => reorderLists(dispatch, mon.getItem().id, id),
        collect: (monitor) => ({
            isOver: !!monitor.isOver(),
        }),
    })

    return (
        <Block
            props={{ ref: _.flow(drag, drop) }}
            visibility={isDragging ? 'hidden' : 'visible'}
            fontSize="large"
            padding="0.3rem 0.5rem"
            fontWeight={selected ? 'bolder' : 'lighter'}
            borderRight={selected ? '5px solid #66f' : undefined}
            backgroundColor={selected ? '#bbb' : undefined}
        >
            <Link href="/local/[id]" as={`/local/${id}`} shallow>
                <a style={{ color: 'black', textDecoration: 'none' }}>
                    {title}
                </a>
            </Link>
        </Block>
    )
}
export const Sidebar = (): ReactElement => {
    const lists = useLists()
    const selected = useSelected()
    const order = useOrder()

    const dispatch = useDispatch()
    const addList = React.useCallback(() => addNewList(dispatch), [dispatch])

    return (
        <Col height="100%" width="25%" backgroundColor="#ddd">
            <Block fontSize="x-large" padding="0.5rem" marginBottom="0.5rem">
                Lists
            </Block>
            <Col overflowY="auto">
                {order.map((id: ListId) => (
                    <ListItem
                        key={id}
                        title={lists[id]?.title}
                        selected={id === selected}
                        id={id}
                    />
                ))}
            </Col>
            <Block alignSelf="center">
                <IconButton onClick={addList}>
                    <AddCircleOutlineIcon />
                </IconButton>
            </Block>
        </Col>
    )
}
