import { createStore, AnyAction } from 'redux'
import { MakeStore, createWrapper } from 'next-redux-wrapper'
import { v4 as uuid } from 'uuid'
import _ from 'lodash'

export type EntryId = string
export interface Entry {
    id: EntryId
    done?: boolean
    text: string
    header?: boolean
}

export type EntryBox = { [id: string]: Entry | undefined }

export type ListId = string
export interface List {
    id: ListId
    title: string
    entries: EntryBox
    order: EntryId[]
}

export type ListBox = { [id: string]: List | undefined }

export interface State {
    selected?: ListId
    lists: ListBox
    order: ListId[]
}

export enum Types {
    addNewEntry,
    addNewHeader,
    addNewList,
    deleteEntry,
    deleteList,
    duplicateList,
    resetEntries,
    reorderEntries,
    reorderLists,
    selectList,
    setListTitle,
    setEntryDone,
    setEntryText,
}

const defaultState = {
    lists: {},
    order: [],
}

export const reducer = (
    state: State = defaultState,
    action: AnyAction
): State => {
    const newState = _.cloneDeep(state)
    switch (action.type) {
        case Types.addNewEntry: {
            const { id } = action
            const list = newState.lists[id]
            if (!list) break

            const entryId = uuid()
            list.order.push(entryId)
            list.entries[entryId] = {
                id: entryId,
                text: '',
            }

            break
        }
        case Types.addNewHeader: {
            const { id } = action
            const list = newState.lists[id]
            if (!list) break

            const entryId = uuid()
            list.order.push(entryId)
            list.entries[entryId] = {
                id: entryId,
                header: true,
                text: 'Header',
            }

            break
        }

        case Types.addNewList: {
            const id = uuid()
            newState.order.push(id)
            newState.lists[id] = {
                id,
                title: 'New List',
                order: [],
                entries: {},
            }

            break
        }
        case Types.deleteEntry: {
            const { listId, entryId } = action
            const list = newState.lists[listId]

            if (!list) break

            const idx = list.order.indexOf(entryId)

            if (idx < 0) break

            delete list.entries[entryId]
            list.order.splice(idx, 1)

            break
        }
        case Types.deleteList: {
            const { id } = action

            const idx = newState.order.indexOf(id)

            delete newState.lists[id]
            newState.order.splice(idx, 1)

            newState.selected = newState.order[Math.max(0, idx - 1)]

            break
        }
        case Types.duplicateList: {
            const { id } = action

            const list = newState.lists[id]
            if (!list) break

            const idx = newState.order.indexOf(id)

            const newId = uuid()
            newState.order.splice(idx + 1, 0, newId)
            newState.lists[newId] = _.cloneDeep({
                ...list,
                id: newId,
                title: `Copy of ${list.title}`,
            })

            break
        }
        case Types.resetEntries: {
            const { id } = action
            const entries = newState.lists[id]?.entries
            if (entries === undefined) break

            Object.keys(entries).forEach((k) => {
                entries[k]!.done = false
            })

            break
        }
        case Types.reorderEntries: {
            const { listId, from, to } = action
            const order = newState.lists[listId]?.order
            if (!order) break

            const fromIdx = order.indexOf(from)
            const toIdx = order.indexOf(to)

            order.splice(fromIdx, 1)
            order.splice(toIdx, 0, from)

            break
        }
        case Types.reorderLists: {
            const { from, to } = action
            const fromIdx = newState.order.indexOf(from)
            const toIdx = newState.order.indexOf(to)

            newState.order.splice(fromIdx, 1)
            newState.order.splice(toIdx, 0, from)

            break
        }
        case Types.selectList: {
            const { id } = action
            newState.selected = id

            break
        }
        case Types.setListTitle: {
            const { id, title } = action
            const list = newState.lists[id]
            if (list) list.title = title

            break
        }
        case Types.setEntryDone: {
            const { listId, entryId, done } = action
            const entry = newState.lists[listId]?.entries[entryId]
            if (entry) entry.done = done

            break
        }
        case Types.setEntryText: {
            const { listId, entryId, text } = action
            const entry = newState.lists[listId]?.entries[entryId]
            if (entry) entry.text = text

            break
        }
    }
    // console.log('===========')
    // console.log('prestate', state)
    // console.log('action', Types[action.type], action)
    // console.log('poststate', newState)
    return newState
}

const makeStore: MakeStore<State> = () => {
    let persistedState = defaultState
    if (typeof window !== 'undefined') {
        const stored = window?.localStorage?.getItem('reduxState')
        persistedState = stored ? JSON.parse(stored) : defaultState
    }

    const store = createStore(reducer, persistedState)

    store.subscribe(() => {
        if (typeof window !== 'undefined') {
            if (window?.localStorage)
                window!.localStorage!.setItem(
                    'reduxState',
                    JSON.stringify(store.getState())
                )
        }
    })

    return store
}

export const wrapper = createWrapper<State>(makeStore, { debug: true })
