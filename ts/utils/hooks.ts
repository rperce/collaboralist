import React from 'react'
import { ListBox, List, ListId, State } from './store'
import { useSelector } from 'react-redux'

type Mapper<T> = (arg0: T) => T
type SetFunction<T> = (arg0: T | Mapper<T>) => void
export type Hook<T> = [T, SetFunction<T>]

export const useLocalStorage = <T>(key: string, fallback: T): Hook<T> => {
    const [storedValue, setStoredValue] = React.useState(() => {
        try {
            const item = window.localStorage.getItem(key)

            return item ? (JSON.parse(item) as T) || fallback : fallback
        } catch (error) {
            console.log(error)

            return fallback
        }
    })

    const setValue = (value: T | Mapper<T>): void => {
        try {
            const valueToStore =
                value instanceof Function ? value(storedValue) : value

            window.localStorage.setItem(key, JSON.stringify(valueToStore))

            setStoredValue(valueToStore)
        } catch (error) {
            console.log(error)
        }
    }

    return [storedValue, setValue]
}

export const useLists = (): ListBox => {
    return useSelector<State, ListBox>((state) => state.lists)
}

export const useList = (id: ListId | undefined): List | undefined => {
    return id
        ? useSelector<State, List | undefined>((state) => state.lists[id])
        : undefined
}

export const useSelected = (): ListId | undefined => {
    return useSelector<State, ListId | undefined>((state) => state.selected)
}

export const useOrder = (): ListId[] => {
    return useSelector<State, ListId[]>((state) => state.order)
}
