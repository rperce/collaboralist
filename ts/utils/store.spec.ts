import { expect } from 'chai'
import { Entry, EntryId, List, ListId, State, Types, reducer } from './store'
import _ from 'lodash'

const findList = (state: State, f: (_: List) => boolean): List => {
    const list = _.find(state.lists, (x) => (x ? f(x) : false))
    expect(list).not.to.be.undefined

    return list!
}

const getList = (state: State, id: ListId): List => {
    expect(id in state.lists).to.be.true

    return state.lists[id]!
}

const findEntry = (list: List, f: (_: Entry) => boolean): Entry => {
    const entry = _.find(list.entries, (x) => (x ? f(x) : false))
    expect(entry).not.to.be.undefined

    return entry!
}

const getEntry = (list: List, id: EntryId): Entry => {
    expect(id in list.entries).to.be.true

    return list.entries[id]!
}

describe('reducer', () => {
    const initialEmpty: State = {
        lists: {},
        order: [],
    }
    const initialFoo: State = {
        lists: {
            foo: {
                id: 'foo',
                title: 'Foo',
                entries: {
                    '1': { id: '1', text: 'Entry' },
                },
                order: ['1'],
            },
        },
        order: ['foo'],
    }
    const initialBarBaz: State = {
        lists: {
            bar: {
                id: 'bar',
                title: 'Bar',
                entries: {
                    b: { id: 'b', text: 'bar-b' },
                    a: { id: 'a', text: 'bar-a', header: true },
                },
                order: ['a', 'b'],
            },

            baz: {
                id: 'baz',
                title: 'Baz',
                entries: {
                    a: { id: 'a', text: 'baz-a', header: true },
                    c: { id: 'c', text: 'baz-c' },
                    b: { id: 'b', text: 'baz-b' },
                },
                order: ['a', 'b', 'c'],
            },
        },
        order: ['bar', 'baz'],
    }
    describe('Types.addNewEntry', () => {
        const action = { type: Types.addNewEntry, id: 'foo' }
        it('does not change state when id does not exist', () => {
            expect(reducer(initialEmpty, action)).to.eql(initialEmpty)
        })
        it('adds new entry to existing list', () => {
            const result = reducer(initialFoo, action)

            const list = getList(result, 'foo')

            const entry = findEntry(list, (x) => x.id !== '1')

            expect(entry).to.eql({
                id: entry.id,
                text: '',
            })

            expect(list.order).to.eql(['1', entry.id])
        })
    })
    describe('Types.addNewHeader', () => {
        const action = { type: Types.addNewHeader, id: 'foo' }
        it('does not change state when id does not exist', () => {
            expect(reducer(initialEmpty, action)).to.eql(initialEmpty)
        })
        it('adds new header to existing list', () => {
            const result = reducer(initialFoo, action)

            const list = getList(result, 'foo')

            const entry = findEntry(list, (x) => x.id !== '1')

            expect(entry).to.eql({
                id: entry.id,
                header: true,
                text: 'Header',
            })

            expect(list.order).to.eql(['1', entry.id])
        })
    })
    describe('Types.addNewList', () => {
        const action = { type: Types.addNewList }
        it('adds a list when starting from empty', () => {
            const result = reducer(initialEmpty, action)
            const list = findList(result, () => true)

            expect(list).to.eql({
                id: list.id,
                title: 'New List',
                entries: {},
                order: [],
            })
        })
        it('adds a list when starting from existing state', () => {
            const result = reducer(initialFoo, action)
            const list = findList(result, (l) => l.id !== 'foo')

            expect(list).to.eql({
                id: list.id,
                title: 'New List',
                entries: {},
                order: [],
            })
        })
    })
    describe('Types.deleteEntry', () => {
        const action = { type: Types.deleteEntry, listId: 'foo', entryId: '1' }
        it('does not change state if the listId does not match', () => {
            const result = reducer(initialEmpty, action)
            expect(result).to.eql(initialEmpty)
        })
        it('does not change state if the entryId does not match', () => {
            const result = reducer(initialFoo, { ...action, entryId: '2' })
            expect(result).to.eql(initialFoo)
        })
        it('deletes matching entry', () => {
            const result = reducer(initialFoo, action)
            const list = getList(result, 'foo')
            expect(list).to.eql({
                id: 'foo',
                title: 'Foo',
                entries: {},
                order: [],
            })
        })
    })
    describe('Types.deleteList', () => {
        const action = { type: Types.deleteList, id: 'foo' }
        it('does not change state if the listId does not match', () => {
            const result = reducer(initialEmpty, action)
            expect(result).to.eql({ ...initialEmpty, selected: undefined })
        })
        it('deletes matching entry', () => {
            const result = reducer(initialFoo, action)
            expect(result).to.eql({ ...initialEmpty, selected: undefined })
        })
    })
    describe('Types.duplicateList', () => {
        const action = { type: Types.duplicateList, id: 'foo' }
        it('does not change state if the listId does not match', () => {
            const result = reducer(initialEmpty, action)
            expect(result).to.eql(initialEmpty)
        })
        it('duplicates matching list', () => {
            const result = reducer(initialFoo, action)
            const list = findList(result, (x) => x.id !== 'foo')
            expect(list).to.eql({
                id: list.id,
                title: 'Copy of Foo',
                entries: {
                    '1': { id: '1', text: 'Entry' },
                },
                order: ['1'],
            })
        })
    })
    describe('Types.resetEntries', () => {
        const action = { type: Types.resetEntries, id: 'foo' }
        it('does not change state if the listId does not match', () => {
            const result = reducer(initialEmpty, action)
            expect(result).to.eql(initialEmpty)
        })
        it('sets done to false in entries of matching list', () => {
            const result = reducer(initialFoo, action)
            const list = getList(result, 'foo')
            const entry = getEntry(list, '1')
            expect(entry).to.eql({
                id: '1',
                text: 'Entry',
                done: false,
            })
        })
    })
    describe('Types.reorderEntries', () => {
        const type = Types.reorderEntries
        it('does not change state if listId does not match', () => {
            const action = { type, listId: 'foo', from: '1', to: '1' }
            const result = reducer(initialEmpty, action)
            expect(result).to.eql(initialEmpty)
        })
        it('does not change state if dropped onto itself', () => {
            const action = { type, listId: 'baz', from: 'a', to: 'a' }
            const result = reducer(initialBarBaz, action)
            expect(result).to.eql(initialBarBaz)
        })
        it('can move entry later', () => {
            const action = { type, listId: 'baz', from: 'a', to: 'c' }
            const result = reducer(initialBarBaz, action)
            const list = getList(result, 'baz')
            expect(list.order).to.eql(['b', 'c', 'a'])
        })
        it('can move entry earlier', () => {
            const action = { type, listId: 'baz', from: 'c', to: 'a' }
            const result = reducer(initialBarBaz, action)
            const list = getList(result, 'baz')
            expect(list.order).to.eql(['c', 'a', 'b'])
        })
    })
    describe('Types.reorderLists', () => {
        const type = Types.reorderLists
        it('does not change state if dropped onto itself', () => {
            const action = { type, from: 'bar', to: 'bar' }
            const result = reducer(initialBarBaz, action)
            expect(result).to.eql(initialBarBaz)
        })
        it('can move entry later', () => {
            const action = { type, from: 'bar', to: 'baz' }
            const result = reducer(initialBarBaz, action)
            expect(result.order).to.eql(['baz', 'bar'])
        })
        it('can move entry earlier', () => {
            const action = { type, from: 'baz', to: 'bar' }
            const result = reducer(initialBarBaz, action)
            expect(result.order).to.eql(['baz', 'bar'])
        })
    })
    describe('Types.selectList', () => {
        it('selects the passed id, regardless of value', () => {
            const action = { type: Types.selectList, id: 'test-string' }
            const result = reducer(initialFoo, action)
            expect(result.selected).to.eql('test-string')
        })
    })
    describe('Types.setListTitle', () => {
        const action = { type: Types.setListTitle, id: 'foo', title: 'Wow' }
        it('does not change state if listId does not match', () => {
            const result = reducer(initialBarBaz, action)
            expect(result).to.eql(initialBarBaz)
        })
        it('sets list title', () => {
            const result = reducer(initialFoo, action)
            const list = getList(result, 'foo')
            expect(list.title).to.eql('Wow')
        })
    })
    describe('Types.setEntryDone', () => {
        const action = { type: Types.setEntryDone, listId: 'foo', entryId: '1' }
        it('does not change state if listId does not match', () => {
            const result = reducer(initialBarBaz, action)
            expect(result).to.eql(initialBarBaz)
        })
        it('sets done to true', () => {
            const result = reducer(initialFoo, { ...action, done: true })
            const list = getList(result, 'foo')
            const entry = getEntry(list, '1')
            expect(entry.done).to.be.true
        })
        it('sets done to false', () => {
            const result = reducer(initialFoo, { ...action, done: false })
            const list = getList(result, 'foo')
            const entry = getEntry(list, '1')
            expect(entry.done).to.be.false
        })
    })
    describe('Types.setEntryText', () => {
        const action = { type: Types.setEntryText, listId: 'foo', entryId: '1' }
        it('does not change state if listId does not match', () => {
            const result = reducer(initialBarBaz, action)
            expect(result).to.eql(initialBarBaz)
        })
        it('sets test', () => {
            const result = reducer(initialFoo, { ...action, text: 'beep boop' })
            const list = getList(result, 'foo')
            const entry = getEntry(list, '1')
            expect(entry.text).to.eql('beep boop')
        })
    })
})
