import { Types, ListId, EntryId } from './store'
import { Dispatch } from 'redux'

export const selectList = (dispatch: Dispatch, id: ListId): void => {
    dispatch({ type: Types.selectList, id })
}

export const addNewEntry = (dispatch: Dispatch, id: ListId): void => {
    dispatch({ type: Types.addNewEntry, id })
}

export const addNewHeader = (dispatch: Dispatch, id: ListId): void => {
    dispatch({ type: Types.addNewHeader, id })
}

export const addNewList = (dispatch: Dispatch): void => {
    dispatch({ type: Types.addNewList })
}

export const deleteEntry = (
    dispatch: Dispatch,
    listId: ListId,
    entryId: EntryId
): void => {
    dispatch({ type: Types.deleteEntry, listId, entryId })
}

export const deleteList = (dispatch: Dispatch, id: ListId): void => {
    dispatch({ type: Types.deleteList, id })
}

export const duplicateList = (dispatch: Dispatch, id: ListId): void => {
    dispatch({ type: Types.duplicateList, id })
}

export const resetEntries = (dispatch: Dispatch, id: ListId): void => {
    dispatch({ type: Types.resetEntries, id })
}

export const setListTitle = (
    dispatch: Dispatch,
    id: ListId,
    title: string
): void => {
    dispatch({ type: Types.setListTitle, id, title })
}

export const setEntryDone = (
    dispatch: Dispatch,
    listId: ListId,
    entryId: EntryId,
    done: boolean
): void => {
    dispatch({ type: Types.setEntryDone, listId, entryId, done })
}

export const setEntryText = (
    dispatch: Dispatch,
    listId: ListId,
    entryId: EntryId,
    text: string
): void => {
    dispatch({ type: Types.setEntryText, listId, entryId, text })
}

export const reorderLists = (
    dispatch: Dispatch,
    from: ListId,
    to: ListId
): void => {
    dispatch({ type: Types.reorderLists, from, to })
}

export const reorderEntries = (
    dispatch: Dispatch,
    listId: ListId,
    from: EntryId,
    to: EntryId
): void => {
    dispatch({ type: Types.reorderEntries, listId, from, to })
}
