import React from 'react'
import Head from 'next/head'
import Router from 'next/router'
import { Block, Row } from 'jsxstyle'
import { Sidebar } from '../ts/components/Sidebar'
import { normalizeCSS } from '../ts/utils/normalize'
import { useOrder } from '../ts/utils/hooks'

export default function Home(): React.ReactElement {
    const order = useOrder()

    React.useEffect(() => {
        if (order.length > 0) {
            Router.replace('/local/[id]', `/local/${order[0]}`)
        }
    }, [order])

    return (
        <div className="container">
            <Head>
                <title>Collaboralist</title>
                <link rel="icon" href="/favicon.ico" />
                <link
                    href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap"
                    rel="stylesheet"
                />
                <style>{normalizeCSS}</style>
            </Head>
            <Row height="100vh">
                <Sidebar />
                <Block>
                    {order.length === 0
                        ? 'No lists! Try creating one with the button on the left!'
                        : 'Selecting first list...'}
                </Block>
            </Row>
        </div>
    )
}
