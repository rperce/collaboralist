import React, { FC } from 'react'
import { AppProps } from 'next/app'
import { wrapper } from '../ts/utils/store'

import { DndProvider } from 'react-dnd'
// import { TouchBackend } from 'react-dnd-touch-backend'
import { HTML5Backend } from 'react-dnd-html5-backend'

const WrappedApp: FC<AppProps> = ({ Component, pageProps }) => (
    <DndProvider backend={HTML5Backend}>
        <Component {...pageProps} />
    </DndProvider>
)

export default wrapper.withRedux(WrappedApp)
