import React from 'react'
import Head from 'next/head'
import Router from 'next/router'
import { useRouter } from 'next/router'
import { Col, Row } from 'jsxstyle'
import { useDispatch } from 'react-redux'
import { Sidebar } from '../../ts/components/Sidebar'
import { ListView } from '../../ts/components/ListView'
import { normalizeCSS } from '../../ts/utils/normalize'
import { selectList } from '../../ts/utils/actions'
import { useOrder } from '../../ts/utils/hooks'

const FourOhFour = () => {
    return (
        <Col alignItems="center" fontSize="1000%">
            404
        </Col>
    )
}

export default function Local(): React.ReactElement {
    const id = useRouter().query.id as string
    const order = useOrder()

    const dispatch = useDispatch()
    React.useEffect(() => {
        if (order.length === 0) Router.replace('/')
        selectList(dispatch, id)
    }, [dispatch, id, order.length])

    return (
        <div className="container">
            <Head>
                <title>Collaboralist</title>
                <link rel="icon" href="/favicon.ico" />
                <link
                    href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap"
                    rel="stylesheet"
                />
                <style>{normalizeCSS}</style>
            </Head>
            <Row height="100vh">
                <Sidebar />
                {order.indexOf(id) >= 0 ? <ListView /> : <FourOhFour />}
            </Row>
        </div>
    )
}
