# Collaboralist

This is the repository for the to-do list application I've always wanted.

## Goals
- Basic to-do list functionality
    - Create/reorder/delete multiple lists
    - Create/reorder/delete/check off/undo entries in a list
- Place "header" items within a list to break it into sections
- Reset all entries to incomplete for a given list
- Looks nice, responsive
- Self-hostable
- Public cloud-hosted option at collaborali.st
- Default list storage uses `localStorage`; in that environment, nothing is sent to a
  server at all
- Toggle to share list publicly, accessible via URL; anyone with the link can add and
  complete items
- Ability (but not requirement) to create an account to share lists with other users or
  share publicly with restricted permissions (e.g. share URL, but non-logged-in users
  can't delete entries, only check them off)
- Open source
- API availability and documentation so users can script their own automation/improvements

## Non-goals
- Running ads
- Requiring login
- Native mobile/desktop applications
- Support for non-javascript clients
- Third-party analytics/trackers

## Roadmap

#### Pre-availablity
- [x] Basic to-do list functionality
- [x] Header items
- [x] Reset entries
- [x] Local-storage mode
- [x] Looks reasonably nice
- [ ] Publicly shared mode
- [ ] Fully keyboard-controllable
- [ ] Responsive (in particular, using a nice library for the drawer)
- [ ] Self-hosting instructions in the README
- [ ] Cloud-hosted option at collaborali.st

#### Post-release
- [ ] User-settable list colors/themes
- [ ] Account creation and related features
- [ ] API documentation
- [ ] Ability to accept donations
- [ ] Show HN?
